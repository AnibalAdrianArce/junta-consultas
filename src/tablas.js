import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(merito, puntaje, docente, orden) {
  return {merito, puntaje, docente, orden};
}

const rows = [
  createData("D",6.0,"Frozen yoghurt", 159),
  createData("H",6.0,"Ice cream sandwich", 237),
  createData("D",6.0,"Eclair", 262),
  createData("H",6.0,"Cupcake",300),
  createData("D",6.0,"Gingerbread", 356),
];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function CustomizedTables() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Docente</StyledTableCell>
            <StyledTableCell align="right">Mérito</StyledTableCell>
            <StyledTableCell align="right">Puntaje</StyledTableCell>
            <StyledTableCell align="right">orden</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name}>
              <StyledTableCell component="th" scope="row">
                {row.docente}
              </StyledTableCell>
              <StyledTableCell align="right">{row.merito}</StyledTableCell>
              <StyledTableCell align="right">{row.puntaje}</StyledTableCell>
              <StyledTableCell align="right">{row.orden}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

import React from 'react';
class Input extends React.Component {
    constructor(props) {
      super(props)
      this.state = {}
    }
  
    render() {
      const {placeholder, valor, setValor, id,readOnly} = this.props
      return (
          <input
            id={id}
            type="text"
            className="input"
            readOnly={readOnly}
            placeholder={placeholder}
            value={valor}
            onChange={(e) => setValor(e.target.value)} //METODO "CONTROLADO"
          />
      );
    }
  }
  export default Input;
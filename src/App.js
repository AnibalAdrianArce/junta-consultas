import React,{Component} from "react";

import botonAgregar from "./comment_user_add_32.png";
import botonImprimir from "./imprimir.png";
import botonBuscar from "././search_button_32.png";
import Input from './input.js';
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";


import "./App.css";
const logo = require('./escudoJCD.png');
var data = require('./Datos.json');

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    height:3,
    padding:2,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.active,
    }
  },      height:3,
      padding:1,
}))(TableRow);

function createData(merito, puntaje, docente,orden) {
  return { merito, puntaje, docente,orden};
}






class App extends Component {

  constructor(props) {
    super(props)
    this.title = React.createRef()
    this.state = {
      docente: "",
      espacio: "",
      codEspacio: "",
      colegio:"",
      llamado:"",

 rows : []
    }

  }

  buscarEspacio(){
    let {codEspacio,espacio,rows} = this.state;
    let encontrado;

    encontrado=data.espacios.find(espacios => espacios.codigo == codEspacio);
    if(encontrado){
         espacio=encontrado.descripcion;
    }
    else{
      espacio='Espacio no encontrado';
    }
    this.setState({espacio});
    rows=[];
    this.setState({rows});

  }

  verNombre(){
    let {docente, codEspacio,rows} = this.state;
    let encontrado,doc_cod, merito, puntaje,datosDoc,orden;
    encontrado=data.docentes.find(docentes => docentes.Dni == docente);
    if(encontrado){
         doc_cod=encontrado.doc_codigo;
         datosDoc=encontrado.Dni+" - "+encontrado.Docente;
    }
    else{
      doc_cod="0000";
    }

    encontrado=data.inscripcion.filter(inscripcion => inscripcion.cod_espacio == codEspacio).find(inscripcion=>inscripcion.doc_codigo==doc_cod);
    if(encontrado){
         merito=encontrado.merito;
         puntaje=encontrado.puntaje;
    }
    else{
      merito="--";
      puntaje="000000";
    }
    orden="0";
    if(merito=="D"){orden="3";}
    if(merito=="H"){orden="2";}
    if(merito=="S"){orden="1";}
    orden=orden+puntaje;
  //  this.setState({espacio});
  encontrado=rows.find(rows => rows.docente == datosDoc);
  if(encontrado) {
     alert("Docente ya ingresado!")
  }
  else
  {
    rows.push({"merito":merito,"puntaje":puntaje,"docente":datosDoc,"orden":orden});
    rows.sort((a, b) => (a.orden < b.orden ? 1 : a.orden > b.orden ? -1 : 0));
    this.setState({rows});
  }

}
imprimir(){
this.title.current.style.display="none";
window.print();
this.title.current.style.display="inline-block";
}
changeColegio(event){
 let {colegio}=this.state;
 colegio=event.target.value;
 this.setState({colegio});

}
changeLlamado(event){
  let {llamado}=this.state;
  llamado=event.target.value;
  this.setState({llamado});

 }



  render(){
   const {rows,colegio}=this.state;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo"  />
        </header>

        <h3>Módulo de Consultas</h3>
        <table align="center" ref={this.title}>
        <tr><td>Institución</td>
              <td>
              <select name="colegio2" id="colegio2" onChange={(e)=>this.changeColegio(e)} value={this.state.value}>
                <option></option>
              {data.instituciones.map((inst) => (
                  <option value={inst.value}>{inst.label}</option>
              ))}

              </select>

              </td>
          </tr>
          <tr><td>Llamado</td>
              <td>
                  <select name='sellamado'onChange={(e)=>this.changeLlamado(e)} value={this.state.value} >
                    <option value=""></option>
                    <option value="1er Llamado">1er Llamado</option>
                    <option value="2do Llamado">2do Llamado</option>
                  </select>
              </td>
          </tr>
          <tr><td>Espacio curricular o Cargo</td>
              <td>
              <Input
                  id="codEspacio"
                  placeholder="Código"
                  valor={this.state.codEspacio}
                  setValor={(nuevo) => this.setState({codEspacio:nuevo})}
                />
                <Input
                  id="espacio"
                  readOnly={true}
                  placeholder="Espacio Curricular o Cargo"
                  valor={this.state.espacio}
                  setValor={(nuevo) => this.setState({espacio:nuevo})}
                />
              </td>
        </tr>
        <tr><td>DNI del Docente</td>
            <td>
              <Input
                id="docente"
                placeholder="DNI del Docente"
                valor={this.state.docente}
                setValor={(nuevo) => this.setState({docente:nuevo})}
              />
               <img src={botonImprimir} className='imagen' onClick={() => this.imprimir()}/>
               <img src={botonAgregar} className='imagen' onClick={() => this.verNombre()}/>
               <img src={botonBuscar} className='imagen' onClick={() => this.buscarEspacio()}/>
            </td></tr>
        </table>
        <TableContainer component={Paper}>
         <Table name='resultados' id='resultados' aria-label="customized table">
           <TableHead>
             <TableRow>
              <StyledTableCell colspan="5">{this.state.colegio}({this.state.llamado})/{this.state.codEspacio}-{this.state.espacio}</StyledTableCell>
             </TableRow>
             <TableRow>
               <StyledTableCell>Mérito</StyledTableCell>
               <StyledTableCell align="right">Puntaje</StyledTableCell>
               <StyledTableCell align="left">Docente</StyledTableCell>
               <StyledTableCell align="right">Acepta</StyledTableCell>
               <StyledTableCell align="left">Firma</StyledTableCell>
             </TableRow>
           </TableHead>
           <TableBody>
             {rows.map((row) => (
               <StyledTableRow key={row.name}>
                 <StyledTableCell align="right" Width="10px">{row.merito}</StyledTableCell>
                 <StyledTableCell align="right" Width="10px">{row.puntaje}</StyledTableCell>
                 <StyledTableCell align="Left">{row.docente}</StyledTableCell>
                 <StyledTableCell align="right" Width="10px">...</StyledTableCell>
                 <StyledTableCell align="Left">..............................</StyledTableCell>
               </StyledTableRow>
             ))}
           </TableBody>
           <TableRow>
             <StyledTableCell colspan="5" align="right">{Date()}</StyledTableCell>
             </TableRow>
         </Table>
       </TableContainer>
      </div>
    );
  }
}

export default App;
